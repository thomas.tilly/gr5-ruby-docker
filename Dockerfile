FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /monsite
WORKDIR /monsite

ADD Gemfile /monsite/Gemfile
ADD Gemfile.lock /monsite/Gemfile.lock
RUN bundle install
ADD . /monsite